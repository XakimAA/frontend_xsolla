Для того, чтобы запустить проект Вам необходимо выполнить следующие шаги:

1. Скачайте файлы в формате .zip (Download -> Download .zip).
2. Разархивируйте в нужную Вам папку.
3. Откройте папку frontend_xsolla-master
4. Запустите файл index.html двойным щелчком мыши. Или можете нажать на него правой кнопкой и открыть с помощью Вашего браузера.